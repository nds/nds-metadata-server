//
// Tools for accessing and serving out gravitational wave data over the nds protocols
//
// Copyright 2020 California Institute of Technology.
//
// This program is free software and is distributed in the hope that it will be
// useful; you may redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software Foundation
// (http://www.gnu.org/licenses); version 2 (GPLv2) of the License or at
// your discretion, any later version (i.e GPLv3).
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
// ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Neither the names of the California Institute of Technology (Caltech), The
// Massachusetts Institute of Technology (M.I.T), The Laser Interferometer
// Gravitational-Wave Observatory (LIGO), nor the names of its contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
//
// You should have received a copy of the licensing terms for this software
// included in the file “LICENSE” located in the top-leveldirectory of this
// package.If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE.txt
//

// Common defines a series of data types and functions used through the nds meta data server
package common

import (
	"encoding/json"
	"fmt"
	"strconv"
)

const (
	DataTypeUnknown   = 0xff
	DataTypeInt16     = 0x1
	DataTypeInt32     = 0x2
	DataTypeInt64     = 0x4
	DataTypeFloat32   = 0x8
	DataTypeFloat64   = 0x10
	DataTypeComplex32 = 0x20
	DataTypeUint32    = 0x40
)

type DataType struct {
	Type byte
}

func CreateDataType(val byte) DataType {
	var dt DataType
	dt.Set(val)
	return dt
}

func (dt *DataType) Set(val byte) {
	switch val {
	case DataTypeInt16:
		dt.Type = DataTypeInt16
	case DataTypeInt32:
		dt.Type = DataTypeInt32
	case DataTypeInt64:
		dt.Type = DataTypeInt64
	case DataTypeFloat32:
		dt.Type = DataTypeFloat32
	case DataTypeFloat64:
		dt.Type = DataTypeFloat64
	case DataTypeComplex32:
		dt.Type = DataTypeComplex32
	case DataTypeUint32:
		dt.Type = DataTypeUint32
	default:
		dt.Type = DataTypeUnknown
	}
}

func (dt DataType) CompatibleWith(other DataType) bool {
	return (byte(dt.Type) & byte(other.Type)) != 0
}

func (dt *DataType) SetString(val string) {
	switch val {
	case "int_2":
		dt.Type = DataTypeInt16
	case "int_4":
		dt.Type = DataTypeInt32
	case "int_8":
		dt.Type = DataTypeInt64
	case "real_4":
		dt.Type = DataTypeFloat32
	case "real_8":
		dt.Type = DataTypeFloat64
	case "complex_8":
		dt.Type = DataTypeComplex32
	case "uint_4":
		dt.Type = DataTypeUint32
	default:
		dt.Type = DataTypeUnknown
	}
}

func (dt DataType) String() string {
	switch dt.Type {
	case DataTypeInt16:
		return "int_2"
	case DataTypeInt32:
		return "int_4"
	case DataTypeInt64:
		return "int_8"
	case DataTypeFloat32:
		return "real_4"
	case DataTypeFloat64:
		return "real_8"
	case DataTypeComplex32:
		return "complex_8"
	case DataTypeUint32:
		return "uint_4"
	default:
		return "unknown"
	}
}

func (dt DataType) MarshalJSON() ([]byte, error) {
	return []byte(strconv.Itoa(int(dt.Type))), nil
}

func (dt *DataType) UnmarshalJSON(data []byte) error {
	if val, err := strconv.ParseUint(string(data), 10, 8); err != nil {
		return err
	} else {
		dt.Set(byte(val))
		return nil
	}
}

const (
	ClassTypeUnknown = 0xff
	ClassTypeOnline  = 0x01
	ClassTypeRaw     = 0x02
	ClassTypeReduced = 0x04
	ClassTypeSTrend  = 0x08
	ClassTypeMTrend  = 0x10
	ClassTypeTestPt  = 0x20
	ClassTypeStatic  = 0x40
)

type ClassType struct {
	Type byte
}

func CreateClassType(val byte) ClassType {
	var ct ClassType
	ct.Set(val)
	return ct
}

func (ct *ClassType) Set(val byte) {
	switch val {
	case ClassTypeOnline:
		ct.Type = ClassTypeOnline
	case ClassTypeRaw:
		ct.Type = ClassTypeRaw
	case ClassTypeReduced:
		ct.Type = ClassTypeReduced
	case ClassTypeSTrend:
		ct.Type = ClassTypeSTrend
	case ClassTypeMTrend:
		ct.Type = ClassTypeMTrend
	case ClassTypeTestPt:
		ct.Type = ClassTypeTestPt
	case ClassTypeStatic:
		ct.Type = ClassTypeStatic
	default:
		ct.Type = ClassTypeUnknown
	}
}

func (ct ClassType) CompatibleWith(other ClassType) bool {
	return (byte(ct.Type) & byte(other.Type)) != 0
}

func (ct *ClassType) SetString(val string) {
	switch val {
	case "online":
		ct.Type = ClassTypeOnline
	case "raw":
		ct.Type = ClassTypeRaw
	case "reduced":
		ct.Type = ClassTypeReduced
	case "s-trend":
		ct.Type = ClassTypeSTrend
	case "m-trend":
		ct.Type = ClassTypeMTrend
	case "test-pt":
		ct.Type = ClassTypeTestPt
	case "static":
		ct.Type = ClassTypeStatic
	default:
		ct.Type = ClassTypeUnknown
	}
}

func (ct ClassType) String() string {
	switch ct.Type {
	case ClassTypeOnline:
		return "online"
	case ClassTypeRaw:
		return "raw"
	case ClassTypeReduced:
		return "reduced"
	case ClassTypeSTrend:
		return "s-trend"
	case ClassTypeMTrend:
		return "m-trend"
	case ClassTypeTestPt:
		return "test-pt"
	case ClassTypeStatic:
		return "static"
	default:
		return "unknown"
	}
}

func (ct ClassType) MarshalJSON() ([]byte, error) {
	return []byte(strconv.Itoa(int(ct.Type))), nil
}

func (ct *ClassType) UnmarshalJSON(data []byte) error {
	if val, err := strconv.ParseUint(string(data), 10, 8); err != nil {
		return err
	} else {
		ct.Set(byte(val))
		return nil
	}
}

type GpsSecond = int64

const GpsInf GpsSecond = 1999999999

type TimeSpan struct {
	start GpsSecond
	end   GpsSecond
}

func CreateTimeSpan(start GpsSecond, end GpsSecond) TimeSpan {
	return TimeSpan{start, end}
}

func (ts TimeSpan) Start() GpsSecond {
	return ts.start
}

func (ts TimeSpan) End() GpsSecond {
	return ts.end
}

func (ts TimeSpan) WideOpen() bool {
	return ts.Start() == 0 && ts.End() == GpsInf
}

func (ts *TimeSpan) Extend(new_end GpsSecond) GpsSecond {
	ts.end = new_end
	return ts.end
}

func (ts TimeSpan) Contains(second GpsSecond) bool {
	return second >= ts.Start() && second < ts.End()
}

func (ts TimeSpan) MarshalJSON() ([]byte, error) {
	data := [2]int64{ts.Start(), ts.End()}
	return json.Marshal(&data)
}

func (ts *TimeSpan) UnmarshalJSON(data []byte) error {
	var intermediate [2]int64
	if err := json.Unmarshal(data, &intermediate); err != nil {
		return err
	}
	ts.start = intermediate[0]
	ts.end = intermediate[1]
	return nil
}

func TimeSpanAsRangeString(ts TimeSpan) string {
	return fmt.Sprintf("[%d, %d)", ts.Start(), ts.End())
}

// Check to see if t1 overlaps t2 at all
func TimeSpansOverlap(t1 TimeSpan, t2 TimeSpan) bool {
	return !(t1.End() <= t2.Start() || t2.End() <= t1.Start())
}

// Return a TimeSpan that is the intersection of t1, t2
// ie the overlapping portions
//
// Undefined if t1 and t2 do not overlap
func ConstrainTimeSpans(t1 TimeSpan, t2 TimeSpan) TimeSpan {
	return CreateTimeSpan(
		MaxInt64(t1.Start(), t2.Start()),
		MinInt64(t1.End(), t2.End()))
}

// Return a timespan that is the combination of t1 + t2
// the largest span possible taking the given endpoints.
func CombineTimeSpans(t1 TimeSpan, t2 TimeSpan) TimeSpan {

	return TimeSpan{
		MinInt64(t1.Start(), t2.Start()),
		MaxInt64(t1.End(), t2.End())}

}

type AUID = uint64

const UnassignedAuid = 0

// A basic channel structure
type BasicChannel struct {
	Name  string    `json:"n"`
	Type  DataType  `json:"dt"`
	Class ClassType `json:"ct"`
	Rate  float64   `json:"r"`
}
