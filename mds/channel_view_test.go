//
// Tools for accessing and serving out gravitational wave data over the nds protocols
//
// Copyright 2020 California Institute of Technology.
//
// This program is free software and is distributed in the hope that it will be
// useful; you may redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software Foundation
// (http://www.gnu.org/licenses); version 2 (GPLv2) of the License or at
// your discretion, any later version (i.e GPLv3).
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
// ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Neither the names of the California Institute of Technology (Caltech), The
// Massachusetts Institute of Technology (M.I.T), The Laser Interferometer
// Gravitational-Wave Observatory (LIGO), nor the names of its contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
//
// You should have received a copy of the licensing terms for this software
// included in the file “LICENSE” located in the top-leveldirectory of this
// package.If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE.txt
//

package mds

import (
	"git.ligo.org/nds/nds-metadata-server/common"
	"git.ligo.org/nds/nds-metadata-server/data_test"
	"testing"
)

type allChannels struct {
}

func (m *allChannels) Matches(channel *BasicChannel) bool {
	return true
}

type rateMatcher struct {
	Rate float64
}

func (m *rateMatcher) Matches(channel *BasicChannel) bool {
	return channel.Rate == m.Rate
}

func TestDBSubset_ApplyToChannelsIf(t *testing.T) {
	mds, _ := LoadDatabaseFromReplicationStream(data_test.SampleDbReplicationStream1Reader())
	sub := mds.GetChannelView("H1:GDS-CALIB_")

	count := 0

	sub.ApplyToChannelsIf(func(availability *ChannelAndAvailability) {
		count++
	}, &allChannels{}, common.CreateTimeSpan(0, common.GpsInf))
	if count != 10 {
		t.Fail()
		t.Logf("Count wrong on apply to all channels")
	}

	count = 0
	sub.ApplyToChannelsIf(func(availability *ChannelAndAvailability) {
		count++
	}, &rateMatcher{16384}, common.CreateTimeSpan(0, common.GpsInf))
	if count != 1 {
		t.Fail()
		t.Logf("Count wrong on apply to 16k channels")
	}
}

func TestDBSubset_ApplyToChannelsIfConstrained(t *testing.T) {
	mds, _ := LoadDatabaseFromReplicationStream(data_test.SampleDbReplicationStream1Reader())
	sub := mds.GetChannelView("H1:GDS-CALIB_")

	count := 0
	sub.ApplyToChannelsIfConstrained(func(availability *ChannelAndAvailability) {
		count++
	}, &allChannels{}, common.CreateTimeSpan(1100000000, 1200000000))
	if count != 2 {
		t.Fail()
		t.Logf("Count wrong on apply to all channels")
	}

	count = 0
	sub.ApplyToChannelsIfConstrained(func(availability *ChannelAndAvailability) {
		count++
	}, &allChannels{}, common.CreateTimeSpan(1200000000, 1200000001))
	if count != 0 {
		t.Fail()
		t.Logf("Count wrong on apply to all channels")
	}

	count = 0
	sub.ApplyToChannelsIfConstrained(func(availability *ChannelAndAvailability) {
		count++
	}, &allChannels{}, common.CreateTimeSpan(1300000000, 1300000001))
	if count != 2 {
		t.Fail()
		t.Logf("Count wrong on apply to all channels")
	}
}

func TestDBSubset_ResolveChannel(t *testing.T) {
	mds, _ := LoadDatabaseFromReplicationStream(data_test.SampleDbReplicationStream1Reader())
	sub := mds.GetChannelView("")

	//reference common.BasicChannel, epoch common.TimeSpan
	channel, ok := sub.ResolveChannel(common.BasicChannel{
		Name:  "H1:GDS-CALIB_KAPPA_A_REAL",
		Type:  common.CreateDataType(common.DataTypeFloat32),
		Class: common.CreateClassType(common.ClassTypeUnknown),
		Rate:  0,
	}, common.CreateTimeSpan(0, common.GpsInf))

	if !ok {
		t.Fatal("ResolveChannels failed")
	}
	_ = channel
}
