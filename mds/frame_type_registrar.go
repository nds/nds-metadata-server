//
// Tools for accessing and serving out gravitational wave data over the nds protocols
//
// Copyright 2020 California Institute of Technology.
//
// This program is free software and is distributed in the hope that it will be
// useful; you may redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software Foundation
// (http://www.gnu.org/licenses); version 2 (GPLv2) of the License or at
// your discretion, any later version (i.e GPLv3).
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
// ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Neither the names of the California Institute of Technology (Caltech), The
// Massachusetts Institute of Technology (M.I.T), The Laser Interferometer
// Gravitational-Wave Observatory (LIGO), nor the names of its contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
//
// You should have received a copy of the licensing terms for this software
// included in the file “LICENSE” located in the top-leveldirectory of this
// package.If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE.txt
//

package mds

import (
	"bufio"
	"compress/gzip"
	"git.ligo.org/nds/nds-metadata-server/common"
	"git.ligo.org/nds/nds-metadata-server/nds2parser"
	"io"
	"log"
	"os"
	"strings"
)

type FrameTypeRegistrar struct {
	db          map[common.BasicChannel]BasicAvailability
	frameByName map[string]int
	frames      [][]common.TimeSpan
}

func CreateFrameTypeRegistrar() *FrameTypeRegistrar {
	return &FrameTypeRegistrar{make(map[common.BasicChannel]BasicAvailability, 100000),
		make(map[string]int, 50),
		make([][]common.TimeSpan, 0, 50),
	}
}

func (fr *FrameTypeRegistrar) getFrameIndex(frameType string) int {
	if index, ok := fr.frameByName[frameType]; ok {
		return index
	}
	index := len(fr.frameByName)
	fr.frameByName[frameType] = index
	fr.frames = append(fr.frames, make([]common.TimeSpan, 0, 10))
	return index
}

func (fr *FrameTypeRegistrar) AddChannelListFromReader(input io.Reader) {
	reader := bufio.NewScanner(input)
	for reader.Scan() {
		entry := strings.Trim(reader.Text(), " \t")
		if len(entry) == 0 || strings.HasPrefix(entry, "#") {
			continue
		}

		parsedLine, err := nds2parser.ChannelListLine(entry)
		if err != nil {
			log.Println(err)
			continue
		}
		key := &parsedLine.BasicChannel

		existance := parsedLine.Existence()

		coreAvail, ok := fr.db[*key]
		if !ok || coreAvail.Existence.Empty() {
			spans := make([]AvailabilitySpan, 0, len(parsedLine.Spans))
			for _, cur_span := range parsedLine.Spans {
				spans = append(spans, AvailabilitySpan{
					FrameType: fr.getFrameIndex(cur_span.FrameType),
					Time:      CreateTimeSpan(cur_span.Start, cur_span.End),
				})
			}
			coreAvail = BasicAvailability{
				CreateTimeSpan(existance.Start(), existance.End()),
				spans,
			}

		} else {
			// this already exists
			coreAvail.Existence = CombineTimeSpans(coreAvail.Existence,
				CreateTimeSpan(existance.Start(), existance.End()))

			for _, cur_span := range parsedLine.Spans {
				coreAvail.Availability = append(coreAvail.Availability, AvailabilitySpan{
					FrameType: fr.getFrameIndex(cur_span.FrameType),
					Time:      CreateTimeSpan(cur_span.Start, cur_span.End),
				})
			}
		}
		fr.db[*key] = coreAvail
	}
}

func (fr *FrameTypeRegistrar) AddChannelListFile(fname string) {
	log.Printf("Loading channel lists from '%s'\n", fname)
	file, err := os.Open(fname)
	if err != nil {
		log.Printf("Unable to open channel list file '%s' with error %v\n", fname, err)
		return
	}
	defer file.Close()
	var reader io.Reader = file
	if strings.HasSuffix(fname, ".gz") {
		gzReader, err := gzip.NewReader(file)
		if err != nil {
			log.Printf("Unable do gzip reading on '%s', error %s\n", fname, err)
			return
		}
		defer gzReader.Close()
		reader = gzReader
	}
	fr.AddChannelListFromReader(reader)
}
