//
// Tools for accessing and serving out gravitational wave data over the nds protocols
//
// Copyright 2020 California Institute of Technology.
//
// This program is free software and is distributed in the hope that it will be
// useful; you may redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software Foundation
// (http://www.gnu.org/licenses); version 2 (GPLv2) of the License or at
// your discretion, any later version (i.e GPLv3).
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
// ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Neither the names of the California Institute of Technology (Caltech), The
// Massachusetts Institute of Technology (M.I.T), The Laser Interferometer
// Gravitational-Wave Observatory (LIGO), nor the names of its contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
//
// You should have received a copy of the licensing terms for this software
// included in the file “LICENSE” located in the top-leveldirectory of this
// package.If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE.txt
//

package server

import (
	"encoding/binary"
	"errors"
	"fmt"
	"git.ligo.org/nds/nds-metadata-server/nds2parser"
	"hash/crc32"
	"io"
	"sort"
	"strconv"
	"strings"
)

import (
	"git.ligo.org/nds/nds-metadata-server/common"
	"git.ligo.org/nds/nds-metadata-server/mds"
)

const defaultProtocolRevision = 5

// BufferedWriter is an io.Writer that can be flushed.
// This allows the client to batch up writes and be more efficient.
type BufferedWriter interface {
	io.Writer
	Flush() error
}

type Client struct {
	conn         BufferedWriter
	db           *mds.MetaDataStore
	authorized   bool
	revision     int
	currentEpoch common.TimeSpan
	epochs       Epoch
}

func CreateClient(conn BufferedWriter, db *mds.MetaDataStore, epochs Epoch) *Client {
	return &Client{
		conn:         conn,
		db:           db,
		authorized:   false,
		revision:     defaultProtocolRevision,
		currentEpoch: common.CreateTimeSpan(0, common.GpsInf),
		epochs:       epochs,
	}
}

func (c *Client) writeAndFlush(data []byte) {
	c.conn.Write(data)
	c.conn.Flush()
}

func (c *Client) daqdOk() {
	c.writeAndFlush([]byte(DAQD_OK))
}

func (c *Client) syntaxError() {
	c.writeAndFlush([]byte(DAQD_SYNTAX))
}

func (c *Client) daqdDelay() {
	c.writeAndFlush([]byte(DAQD_DELAY))
}

func (c *Client) nds2Int32(val int32) {
	binary.Write(c.conn, binary.BigEndian, &val)
}

func (c *Client) nds2Uint32(val uint32) {
	binary.Write(c.conn, binary.BigEndian, &val)
}

func (c *Client) nds2ByteStream(data []byte) {
	count := int32(len(data))
	c.nds2Int32(count)
	c.conn.Write(data)
}

func (c *Client) nds2String(data string) {
	c.nds2ByteStream([]byte(data))
}

func (c *Client) Process(command *nds2parser.ParsedCommand) error {
	defer c.conn.Flush()

	if !c.authorized {
		if command.Command.Get() == nds2parser.CommandAuthorize {
			c.authorized = true
			c.daqdOk()
		} else {
			c.syntaxError()
		}
		return nil
	}
	if command.Command.Get() == nds2parser.CommandProtocolRevision {
		c.revision = command.Revision
	} else {
		command.Revision = c.revision
	}
	if command.Command.Get() != nds2parser.CommandSetEpoch {
		command.TimeSpan = c.currentEpoch
	}

	switch command.Command.Get() {
	case nds2parser.CommandProtocolVersion:
		return c.handleProtocolVersion()
	case nds2parser.CommandProtocolRevision:
		return c.handleProtocolRevision()
	case nds2parser.CommandQuit:
		return errors.New("quit")
	case nds2parser.CommandNop:
		return nil
	case nds2parser.CommandCountChannels:
		return c.handleCountChannels(command)
	case nds2parser.CommandGetChannels:
		return c.handleGetChannels(command)
	case nds2parser.CommandGetChannelCrc:
		return c.handleGetChannelCrc(command)
	case nds2parser.CommandSetEpoch:
		c.daqdOk()
		c.currentEpoch = command.TimeSpan
		return nil
	case nds2parser.CommandListEpochs:
		return c.handleListEpochs()
	case nds2parser.CommandGetSourceList:
		return c.handleGetSourceListorData(command)
	case nds2parser.CommandGetSourceData:
		return c.handleGetSourceListorData(command)
	}

	c.syntaxError()
	return nil
}

func (c *Client) handleProtocolVersion() error {
	c.daqdOk()
	c.nds2Int32(1)
	return nil
}

func (c *Client) handleProtocolRevision() error {
	c.daqdOk()
	c.nds2Int32(int32(c.revision))
	return nil
}

func (c *Client) buildGetCountPredicate(cmd *nds2parser.ParsedCommand) mds.ChannelPredicate {
	if cmd.Pattern.RePattern == "" || cmd.Pattern.RePattern == "^.*$" {
		return CreateChannelPredicateSimple(cmd.ClassType)
	} else {
		return CreateChannelPredicateFull(&cmd.Pattern, cmd.ClassType)
	}
}

func (c *Client) doChannelCount(pred mds.ChannelPredicate, span common.TimeSpan, subset mds.ChannelView) int32 {
	var count int32

	subset.ApplyToChannelsIf(func(chanAndAvail *mds.ChannelAndAvailability) {
		count++
	}, pred, span)
	return count
}

func calculateGetCountChannelTimespan(timespan common.TimeSpan, gps common.GpsSecond) common.TimeSpan {
	if gps == 0 {
		return timespan
	}
	return common.CreateTimeSpan(gps, gps+1)
}

func (c *Client) handleCountChannels(cmd *nds2parser.ParsedCommand) error {
	c.daqdOk()

	predicate := c.buildGetCountPredicate(cmd)
	subset := c.db.GetChannelView(cmd.Pattern.CommonPrefix)
	timeSpan := calculateGetCountChannelTimespan(cmd.TimeSpan, cmd.GpsTime)
	count := c.doChannelCount(predicate, timeSpan, subset)
	c.nds2Int32(count)
	return nil
}

func (c *Client) handleGetChannels(cmd *nds2parser.ParsedCommand) error {
	c.daqdOk()

	predicate := c.buildGetCountPredicate(cmd)
	subset := c.db.GetChannelView(cmd.Pattern.CommonPrefix)
	timeSpan := calculateGetCountChannelTimespan(cmd.TimeSpan, cmd.GpsTime)
	count := c.doChannelCount(predicate, timeSpan, subset)
	c.nds2Int32(count)

	subset.ApplyToChannelsIf(func(chanAndAvail *mds.ChannelAndAvailability) {
		c.nds2ByteStream([]byte(ChannelToStr(&chanAndAvail.Channel)))
	}, predicate, timeSpan)

	return nil
}

func (c *Client) handleGetChannelCrc(cmd *nds2parser.ParsedCommand) error {
	c.daqdOk()

	hash := crc32.NewIEEE()
	predicate := c.buildGetCountPredicate(cmd)
	subset := c.db.GetChannelView(cmd.Pattern.CommonPrefix)
	subset.ApplyToChannelsIf(func(chanAndAvail *mds.ChannelAndAvailability) {
		_, _ = hash.Write([]byte(ChannelToStr(&chanAndAvail.Channel)))
	}, predicate, cmd.TimeSpan)
	c.nds2Uint32(hash.Sum32())
	return nil
}

func (c *Client) handleListEpochs() error {
	c.daqdOk()
	c.nds2ByteStream(c.epochs)
	return nil
}

func (c *Client) handleGetSourceListorData(cmd *nds2parser.ParsedCommand) error {
	if cmd.Revision >= 7 {
		c.daqdDelay()
	}
	dbView := c.db.GetChannelView("")
	targets := make([]*mds.ChannelAndAvailability, len(cmd.Channels))
	for i, _ := range cmd.Channels {
		var ok bool
		targets[i], ok = dbView.ResolveChannel(cmd.Channels[i], cmd.TimeSpan)
		if !ok {
			c.syntaxError()
			return nil
		}
	}
	c.daqdOk()

	frameView := c.db.FrameView()
	first := true
	var builder strings.Builder
	for _, target := range targets {
		if !first {
			builder.WriteRune(' ')
		}
		first = false
		builder.WriteString(target.Channel.Name)
		builder.WriteString(" {")
		if cmd.Command.Get() == nds2parser.CommandGetSourceList {
			printSourceAvailability(&builder, frameView, target, cmd.TimeSpan)
		} else {
			printDetailedAvailability(&builder, frameView, target, cmd.TimeSpan)
		}
		builder.WriteString("}")
	}
	c.nds2String(builder.String())
	return nil
}

func printSourceAvailability(builder *strings.Builder, fv *mds.FrameView, chanAndAvail *mds.ChannelAndAvailability, epoch common.TimeSpan) {
	first := true
	for i, _ := range chanAndAvail.Avail.Availability {
		roughAvail := &chanAndAvail.Avail.Availability[i]
		roughTime := roughAvail.Time.ToCommon()
		if !common.TimeSpansOverlap(roughTime, epoch) {
			continue
		}
		span := common.ConstrainTimeSpans(roughAvail.Time.ToCommon(), epoch)
		frameType, ok := fv.GetFrameType(roughAvail.FrameType)
		if !ok {
			continue
		}
		if !first {
			builder.WriteRune(' ')
		}
		first = false
		builder.WriteString(frameType)
		builder.WriteRune(':')
		if span.Start() == 0 && span.End() == common.GpsInf {
			builder.WriteString("all")
		} else {
			builder.WriteString(strconv.FormatInt(span.Start(), 10))
			builder.WriteRune('-')
			if span.End() == common.GpsInf {
				builder.WriteString("inf")
			} else {
				builder.WriteString(strconv.FormatInt(span.End(), 10))
			}
		}
	}
}

func printDetailedAvailability(builder *strings.Builder, fv *mds.FrameView, chanAndAvail *mds.ChannelAndAvailability, epoch common.TimeSpan) {
	first := true
	for i, _ := range chanAndAvail.Avail.Availability {
		roughAvail := &chanAndAvail.Avail.Availability[i]

		if !common.TimeSpansOverlap(roughAvail.Time.ToCommon(), epoch) {
			continue
		}
		curEpoch := common.ConstrainTimeSpans(epoch, roughAvail.Time.ToCommon())

		trans, frameType, ok := fv.GetTransactionalSpan(roughAvail.FrameType)
		if !ok {
			continue
		}

		frameExistence := (*trans).Read()
		startIndex := sort.Search(len(frameExistence), func(index int) bool {
			return frameExistence[index].End() > curEpoch.Start()
		})
		if startIndex == len(frameExistence) {
			continue
		}
		curView := frameExistence[startIndex:]
		endIndex := sort.Search(len(curView), func(index int) bool {
			return curView[index].Start() >= curEpoch.End()
		})
		for curIndex, _ := range curView[:endIndex] {
			if !first {
				builder.WriteRune(' ')
			}
			curSpan := curView[curIndex].ToCommon()
			//if !common.TimeSpansOverlap(curSpan, curEpoch) {
			//	continue
			//}
			detailed := common.ConstrainTimeSpans(curSpan, curEpoch)
			first = false
			builder.WriteString(frameType)
			builder.WriteRune(':')
			builder.WriteString(strconv.FormatInt(detailed.Start(), 10))
			builder.WriteRune('-')
			builder.WriteString(strconv.FormatInt(detailed.End(), 10))
		}
	}
}

func ChannelToStr(ch *common.BasicChannel) string {
	nullTerm := [1]byte{0}
	if ch.Rate < 1.0 {
		return fmt.Sprintf("%s %v %.7f %v%s", ch.Name, ch.Class, ch.Rate, ch.Type, nullTerm[:])
	}
	return fmt.Sprintf("%s %v %.0f %v%s", ch.Name, ch.Class, ch.Rate, ch.Type, nullTerm[:])
}
