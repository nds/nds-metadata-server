package server

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"git.ligo.org/nds/nds-metadata-server/common"
	"git.ligo.org/nds/nds-metadata-server/data_test"
	"git.ligo.org/nds/nds-metadata-server/mds"
	"git.ligo.org/nds/nds-metadata-server/nds2parser"
	"io"
	"strings"
	"testing"
)

type UnbufferedWriter struct {
	w io.Writer
}

func NewUnbufferedWriter(w io.Writer) *UnbufferedWriter {
	return &UnbufferedWriter{w: w}
}

func (u *UnbufferedWriter) Write(p []byte) (n int, err error) {
	return u.w.Write(p)
}

func (u *UnbufferedWriter) Flush() error {
	return nil
}

type FlushCountingWriter struct {
	w          *bytes.Buffer
	flushCount int
}

func NewFlushCountingWriter() *FlushCountingWriter {
	return &FlushCountingWriter{w: &bytes.Buffer{}, flushCount: 0}
}

func (f *FlushCountingWriter) Write(p []byte) (n int, err error) {
	return f.w.Write(p)
}

func (f *FlushCountingWriter) Flush() error {
	f.flushCount++
	return nil
}

func (f *FlushCountingWriter) reset() {
	f.w.Reset()
	f.flushCount = 0
}

func createDb(t *testing.T) *mds.MetaDataStore {
	mdb, err := mds.LoadDatabaseFromReplicationStream(data_test.SampleDbReplicationStream4Reader())
	if err != nil {
		t.Fatal(err)
	}
	return mdb
}

// lookupChannel returns a channel by name from the database, no other fields are used
func lookupChannel(name string, mdb *mds.MetaDataStore, t *testing.T) *mds.ChannelAndAvailability {
	chanView := mdb.GetChannelView("")
	ch, ok := chanView.ResolveChannel(common.BasicChannel{
		Name:  name,
		Type:  common.CreateDataType(common.DataTypeUnknown),
		Class: common.CreateClassType(common.ClassTypeUnknown),
		Rate:  0,
	}, common.CreateTimeSpan(0, common.GpsInf))
	if !ok {
		t.Fatal("Unable to find test channel")
	}
	return ch
}

func channelWithName(name string) common.BasicChannel {
	return common.BasicChannel{
		Name:  name,
		Type:  common.CreateDataType(common.DataTypeUnknown),
		Class: common.CreateClassType(common.ClassTypeUnknown),
		Rate:  0,
	}
}

func requireString(reader io.Reader, match string, t *testing.T) {
	buf := make([]byte, len(match))
	if _, err := io.ReadFull(reader, buf); err != nil {
		t.Fatalf("Unable to read string from the reader while matching against '%s', %v", match, err)
	}
	if string(buf) != match {
		t.Fatalf("'%s' != '%s'", string(buf), match)
	}
}

func getInt32BE(reader io.Reader, t *testing.T) int32 {
	var buf [4]byte
	if _, err := io.ReadFull(reader, buf[:]); err != nil {
		t.Fatalf("Unable to read int from the reader %v", err)
	}
	return int32(binary.BigEndian.Uint32(buf[:]))
}

func getPascalString(reader io.Reader, t *testing.T) string {
	size := getInt32BE(reader, t)
	buf := make([]byte, size)
	if _, err := io.ReadFull(reader, buf); err != nil {
		t.Fatalf("Unable to read string from the reader %v", err)
	}
	return string(buf)
}

func requireInt32BE(reader io.Reader, match int32, t *testing.T) {
	if match < 0 {
		t.Fatal("Cannot match against negative ints")
	}
	val := getInt32BE(reader, t)
	if val != match {
		t.Fatalf("%v != %v", val, match)
	}
}

func Test_Process_ProtocolVersion(t *testing.T) {
	output := &bytes.Buffer{}
	mdb := createDb(t)
	client := CreateClient(NewUnbufferedWriter(output), mdb, []byte(""))
	client.authorized = true

	cmd := nds2parser.ParsedCommand{
		Command:   nds2parser.CreateCommand(nds2parser.CommandProtocolVersion),
		Version:   1,
		Revision:  0,
		ClassType: common.ClassType{},
		GpsTime:   0,
		TimeSpan:  common.TimeSpan{},
		Pattern:   nds2parser.BashPattern{},
		Channels:  nil,
	}
	err := client.Process(&cmd)
	if err != nil {
		t.Fatal(err)
	}
	requireString(output, DAQD_OK, t)
	requireInt32BE(output, 1, t)
}

func Test_Process_ProtocolRevision(t *testing.T) {
	output := &bytes.Buffer{}
	mdb := createDb(t)
	client := CreateClient(NewUnbufferedWriter(output), mdb, []byte(""))
	client.authorized = true

	type args struct {
		revision int32
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "5",
			args: args{5},
		},
		{
			name: "5",
			args: args{6},
		},
		{
			name: "5",
			args: args{7},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			cmd := nds2parser.ParsedCommand{
				Command:   nds2parser.CreateCommand(nds2parser.CommandProtocolRevision),
				Version:   1,
				Revision:  int(tt.args.revision),
				ClassType: common.ClassType{},
				GpsTime:   0,
				TimeSpan:  common.TimeSpan{},
				Pattern:   nds2parser.BashPattern{},
				Channels:  nil,
			}
			err := client.Process(&cmd)
			if err != nil {
				t.Fatal(err)
			}
			requireString(output, DAQD_OK, t)
			requireInt32BE(output, tt.args.revision, t)
		})
	}
}

func Test_handleCountChannels_and_getChannels(t *testing.T) {
	output := &bytes.Buffer{}
	mdb := createDb(t)
	client := CreateClient(NewUnbufferedWriter(output), mdb, []byte(""))
	client.authorized = true

	type args struct {
		pattern   string
		classType common.ClassType
	}
	tests := []struct {
		name    string
		args    args
		strings []string
	}{
		{
			name: "all",
			args: args{
				pattern:   "*",
				classType: common.CreateClassType(common.ClassTypeUnknown),
			},
			strings: []string{
				"X2:CHAN1 reduced 16 uint_4",
				"X2:CHAN2 reduced 16 real_4",
				"X2:CHAN3 reduced 16 real_4",
				"X2:CHAN4 reduced 16 real_4",
				"X2:CHAN5 reduced 16 real_4",
				"X2:CHAN6 reduced 16 real_4",
				"X2:CHAN7 reduced 16 real_4",
				"X2:CHAN8 raw 16 real_4",
				"X2:CHAN9 raw 16 real_4",
			},
		},
		{
			name: "with a 3 in the name",
			args: args{
				pattern:   "*3*",
				classType: common.CreateClassType(common.ClassTypeUnknown),
			},
			strings: []string{
				"X2:CHAN3 reduced 16 real_4",
			},
		},
		{
			name: "reduced",
			args: args{
				pattern:   "*",
				classType: common.CreateClassType(common.ClassTypeReduced),
			},
			strings: []string{
				"X2:CHAN1 reduced 16 uint_4",
				"X2:CHAN2 reduced 16 real_4",
				"X2:CHAN3 reduced 16 real_4",
				"X2:CHAN4 reduced 16 real_4",
				"X2:CHAN5 reduced 16 real_4",
				"X2:CHAN6 reduced 16 real_4",
				"X2:CHAN7 reduced 16 real_4",
			},
		},
		{
			name: "reduced",
			args: args{
				pattern:   "*",
				classType: common.CreateClassType(common.ClassTypeRaw),
			},
			strings: []string{
				"X2:CHAN8 raw 16 real_4",
				"X2:CHAN9 raw 16 real_4",
			},
		},
		{
			name: "none",
			args: args{
				pattern:   "not_here",
				classType: common.CreateClassType(common.ClassTypeUnknown),
			},
			strings: []string{},
		},
	}
	for _, tt := range tests {
		pattern, err := nds2parser.ParseBashPattern(tt.args.pattern)
		if err != nil {
			t.Fatalf("Invalid bash pattern in test '%s', %v", tt.args.pattern, err)
		}
		cmd := nds2parser.ParsedCommand{
			Command:   nds2parser.CreateCommand(nds2parser.CommandCountChannels),
			Version:   1,
			Revision:  6,
			ClassType: tt.args.classType,
			GpsTime:   0,
			TimeSpan:  common.CreateTimeSpan(0, common.GpsInf),
			Pattern:   pattern,
			Channels:  nil,
		}
		expected := int32(len(tt.strings))
		t.Run(tt.name, func(t *testing.T) {
			client.Process(&cmd)
			requireString(output, DAQD_OK, t)
			requireInt32BE(output, expected, t)
		})
		cmd.Command = nds2parser.CreateCommand(nds2parser.CommandGetChannels)
		t.Run(tt.name+"_channels", func(t *testing.T) {
			client.Process(&cmd)
			requireString(output, DAQD_OK, t)
			requireInt32BE(output, expected, t)
			for _, s := range tt.strings {
				actual := getPascalString(output, t)
				expectedString := fmt.Sprintf("%s%s", s, []byte{0})
				if actual != expectedString {
					t.Fatalf("'%s' != '%s'", actual, expectedString)
				}
			}
		})
		output.Truncate(0)
	}
}

func Test_handleCountChannels_and_getChannels_with_gpstimes(t *testing.T) {
	output := &bytes.Buffer{}
	mdb := createDb(t)
	client := CreateClient(NewUnbufferedWriter(output), mdb, []byte(""))
	client.authorized = true

	type args struct {
		pattern   string
		gpstime   common.GpsSecond
		classType common.ClassType
	}
	tests := []struct {
		name    string
		args    args
		strings []string
	}{
		{
			name: "all",
			args: args{
				pattern:   "*",
				gpstime:   1000000001,
				classType: common.CreateClassType(common.ClassTypeUnknown),
			},
			strings: []string{
				"X2:CHAN2 reduced 16 real_4",
				"X2:CHAN3 reduced 16 real_4",
				"X2:CHAN4 reduced 16 real_4",
				"X2:CHAN5 reduced 16 real_4",
				"X2:CHAN6 reduced 16 real_4",
				"X2:CHAN7 reduced 16 real_4",
				"X2:CHAN8 raw 16 real_4",
				"X2:CHAN9 raw 16 real_4",
			},
		},
		{
			name: "with a 3 in the name expected to be found",
			args: args{
				pattern:   "*3*",
				gpstime:   1000000001,
				classType: common.CreateClassType(common.ClassTypeUnknown),
			},
			strings: []string{
				"X2:CHAN3 reduced 16 real_4",
			},
		},
		{
			name: "with a 3 in the name expected to not be found",
			args: args{
				pattern:   "*3*",
				gpstime:   1600000000,
				classType: common.CreateClassType(common.ClassTypeUnknown),
			},
			strings: []string{},
		},
	}
	for _, tt := range tests {
		pattern, err := nds2parser.ParseBashPattern(tt.args.pattern)
		if err != nil {
			t.Fatalf("Invalid bash pattern in test '%s', %v", tt.args.pattern, err)
		}
		cmd := nds2parser.ParsedCommand{
			Command:   nds2parser.CreateCommand(nds2parser.CommandCountChannels),
			Version:   1,
			Revision:  6,
			ClassType: tt.args.classType,
			GpsTime:   tt.args.gpstime,
			TimeSpan:  common.CreateTimeSpan(0, common.GpsInf),
			Pattern:   pattern,
			Channels:  nil,
		}
		expected := int32(len(tt.strings))
		t.Run(tt.name, func(t *testing.T) {
			client.Process(&cmd)
			requireString(output, DAQD_OK, t)
			requireInt32BE(output, expected, t)
		})
		cmd.Command = nds2parser.CreateCommand(nds2parser.CommandGetChannels)
		t.Run(tt.name+"_channels", func(t *testing.T) {
			client.Process(&cmd)
			requireString(output, DAQD_OK, t)
			requireInt32BE(output, expected, t)
			for _, s := range tt.strings {
				actual := getPascalString(output, t)
				expectedString := fmt.Sprintf("%s%s", s, []byte{0})
				if actual != expectedString {
					t.Fatalf("'%s' != '%s'", actual, expectedString)
				}
			}
		})
		output.Truncate(0)
	}
}

func Test_handleGetSourceListorData(t *testing.T) {
	output := &bytes.Buffer{}
	mdb := createDb(t)
	client := CreateClient(NewUnbufferedWriter(output), mdb, []byte(""))
	client.authorized = true
	type args struct {
		patterns []string
	}
	tests := []struct {
		name       string
		args       args
		sourceList string
		sourceData string
	}{
		{
			name: "chan1 and chan2",
			args: args{
				patterns: []string{
					"X2:CHAN1",
					"X2:CHAN2",
				},
			},
			sourceList: "X2:CHAN1 {FT1:1100000000-1200000000 FT1:1300000000-inf FT2:1250000000-inf} X2:CHAN2 {FT1:1000000000-inf}",
		},
	}
	for _, tt := range tests {
		channels := make([]common.BasicChannel, 0, len(tt.args.patterns))
		for _, pattern := range tt.args.patterns {
			channels = append(channels, channelWithName(pattern))
		}
		cmd := nds2parser.ParsedCommand{
			Command:   nds2parser.CreateCommand(nds2parser.CommandGetSourceList),
			Version:   1,
			Revision:  6,
			ClassType: common.CreateClassType(common.ClassTypeUnknown),
			GpsTime:   0,
			TimeSpan:  common.CreateTimeSpan(0, common.GpsInf),
			Pattern:   nds2parser.BashPattern{},
			Channels:  channels,
		}
		t.Run(tt.name, func(t *testing.T) {
			client.handleGetSourceListorData(&cmd)
			requireString(output, DAQD_OK, t)
			actual := getPascalString(output, t)
			if actual != tt.sourceList {
				t.Fatalf("'%s' != '%s'", actual, tt.sourceList)
			}
			t.Log(actual)
		})
	}
}

func Test_printDetailedAvailability(t *testing.T) {
	mdb := createDb(t)
	fv := mdb.FrameView()
	ch1 := lookupChannel("X2:CHAN1", mdb, t)

	type args struct {
		chanAndAvail *mds.ChannelAndAvailability
		epoch        common.TimeSpan
		expected     string
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "All availability",
			args: args{
				chanAndAvail: ch1,
				epoch:        common.CreateTimeSpan(0, common.GpsInf),
				expected:     "FT1:1100000000-1200000000 FT1:1300000000-1400000000 FT1:1450000000-1500000000 FT2:1250000000-1350000000 FT2:1380000000-1400000000 FT2:1450000000-1500000000",
			},
		},
		{
			name: "empty epoch",
			args: args{
				chanAndAvail: ch1,
				epoch:        common.CreateTimeSpan(0, 0),
				expected:     "",
			},
		},
		{
			name: "non-overlapping epoch",
			args: args{
				chanAndAvail: ch1,
				epoch:        common.CreateTimeSpan(0, 1000000000),
				expected:     "",
			},
		},
		{
			name: "epoch in gap",
			args: args{
				chanAndAvail: ch1,
				epoch:        common.CreateTimeSpan(1400000000, 1450000000),
				expected:     "",
			},
		},
		{
			name: "partially overlapping epoch",
			args: args{
				chanAndAvail: ch1,
				epoch:        common.CreateTimeSpan(1200000000, common.GpsInf),
				expected:     "FT1:1300000000-1400000000 FT1:1450000000-1500000000 FT2:1250000000-1350000000 FT2:1380000000-1400000000 FT2:1450000000-1500000000",
			},
		},
		{
			name: "partially overlapping epoch, splitting detailed availability",
			args: args{
				chanAndAvail: ch1,
				epoch:        common.CreateTimeSpan(1350000000, common.GpsInf),
				expected:     "FT1:1350000000-1400000000 FT1:1450000000-1500000000 FT2:1380000000-1400000000 FT2:1450000000-1500000000",
			},
		},
		{
			name: "partially overlapping epoch, splitting detailed availability, start at 0",
			args: args{
				chanAndAvail: ch1,
				epoch:        common.CreateTimeSpan(0, 1350000000),
				expected:     "FT1:1100000000-1200000000 FT1:1300000000-1350000000 FT2:1250000000-1350000000",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			output := &strings.Builder{}
			printDetailedAvailability(output, fv, tt.args.chanAndAvail, tt.args.epoch)
			actual := output.String()
			if actual != tt.args.expected {
				t.Fatalf("'%s' != '%s'", actual, tt.args.expected)
			}
		})
	}
}

func Test_printSourceAvailability(t *testing.T) {
	mdb := createDb(t)
	fv := mdb.FrameView()
	ch1 := lookupChannel("X2:CHAN1", mdb, t)
	ch2 := lookupChannel("X2:CHAN2", mdb, t)
	ch3 := lookupChannel("X2:CHAN9", mdb, t)

	type args struct {
		chanAndAvail *mds.ChannelAndAvailability
		epoch        common.TimeSpan
		expected     string
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "basic test with gaps",
			args: args{
				chanAndAvail: ch1,
				epoch:        common.CreateTimeSpan(0, common.GpsInf),
				expected:     "FT1:1100000000-1200000000 FT1:1300000000-inf FT2:1250000000-inf",
			},
		},
		{
			name: "basic test",
			args: args{
				chanAndAvail: ch2,
				epoch:        common.CreateTimeSpan(0, common.GpsInf),
				expected:     "FT1:1000000000-inf",
			},
		},
		{
			name: "always available",
			args: args{
				chanAndAvail: ch3,
				epoch:        common.CreateTimeSpan(0, common.GpsInf),
				expected:     "FT1:all",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			output := &strings.Builder{}
			printSourceAvailability(output, fv, tt.args.chanAndAvail, tt.args.epoch)
			actual := output.String()
			if actual != tt.args.expected {
				t.Fatalf("'%s' != '%s'", actual, tt.args.expected)
			}
		})
	}
}

func TestClient_writeAndFlush(t *testing.T) {
	output := NewFlushCountingWriter()
	mdb := createDb(t)
	client := CreateClient(output, mdb, []byte(""))

	output.flushCount = 0
	client.writeAndFlush([]byte("hello"))
	if output.w.String() != "hello" {
		t.Fatal("writeAndFlush did not fill the buffer as expected")
	}
	if output.flushCount != 1 {
		t.Fatalf("writeAndFlush flush count != 1")
	}
}

func TestClient_DaqStatusAreFlushed(t *testing.T) {
	mdb := createDb(t)
	output := NewFlushCountingWriter()
	client := CreateClient(output, mdb, []byte(""))

	type args struct {
		f        func()
		expected string
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "daqdOk",
			args: args{
				f: func() {
					client.daqdOk()
				},
				expected: DAQD_OK,
			},
		},
		{
			name: "syntaxError",
			args: args{
				f: func() {
					client.syntaxError()
				},
				expected: DAQD_SYNTAX,
			},
		},
		{
			name: "daqdDelay",
			args: args{
				f: func() {
					client.daqdDelay()
				},
				expected: DAQD_DELAY,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			output.reset()
			tt.args.f()
			if output.w.String() != tt.args.expected {
				t.Fatalf("Output buffer did not contain, %s", tt.args.expected)
			}
			if output.flushCount != 1 {
				t.Fatal("Output flush count != 1")
			}
		})
	}
}
