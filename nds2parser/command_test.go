//
// Tools for accessing and serving out gravitational wave data over the nds protocols
//
// Copyright 2020 California Institute of Technology.
//
// This program is free software and is distributed in the hope that it will be
// useful; you may redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software Foundation
// (http://www.gnu.org/licenses); version 2 (GPLv2) of the License or at
// your discretion, any later version (i.e GPLv3).
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
// ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Neither the names of the California Institute of Technology (Caltech), The
// Massachusetts Institute of Technology (M.I.T), The Laser Interferometer
// Gravitational-Wave Observatory (LIGO), nor the names of its contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
//
// You should have received a copy of the licensing terms for this software
// included in the file “LICENSE” located in the top-leveldirectory of this
// package.If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE.txt
//

package nds2parser

import (
	"git.ligo.org/nds/nds-metadata-server/common"
	"testing"
)

func TestParseNDS2SimpleCommands(t *testing.T) {
	type TestData struct {
		Input    string
		Command  int
		Revision int
	}

	TestsCases := []TestData{
		{"authorize", CommandAuthorize, 5},
		{"Authorize", CommandUnknown, 5},
		{"authorized", CommandUnknown, 5},
		{" authorize", CommandAuthorize, 5},
		{"authorize  ", CommandAuthorize, 5},
		{" authorize  ", CommandAuthorize, 5},
		{"server-protocol-version;",
			CommandProtocolVersion, 5},
		{" server-protocol-version; ",
			CommandProtocolVersion, 5},
		{" server-protocol-version ; ", CommandUnknown, 5},
		{" server-protocol-revision;",
			CommandProtocolRevision, 5},
		{" server-protocol-revision 6;",
			CommandProtocolRevision,
			6},
		{" server-protocol-revision 7;",
			CommandProtocolRevision,
			7},
		{"server-protocol-revision;",
			CommandProtocolRevision, 5},
		{" server-protocol-revision ;", CommandUnknown, 5},
		{" server-protocol-revision ; ", CommandUnknown, 5},
		{"quit;", CommandQuit, 5},
		{" quit;", CommandQuit, 5},
		{"quit; ", CommandQuit, 5},
		{" quit; ", CommandQuit, 5},
		{"quit ;", CommandUnknown, 5},
		{" quit ;", CommandUnknown, 5},
		{" quit ; ", CommandUnknown, 5},
		{"foo", CommandUnknown, 5},
		{"", CommandNop, 5},
		{" ", CommandNop, 5},
		{"\t", CommandNop, 5},
		{" \t", CommandNop, 5},
		{" \t ", CommandNop, 5},
		{"\t ", CommandNop, 5},
		{"\t \t", CommandNop, 5},
		{"list-epochs;", CommandListEpochs, 5},
		{"set-epoch 1000000300-1000000200;", CommandUnknown, 5},
	}

	for _, testCase := range TestsCases {
		cmd := ParseCommand(testCase.Input)
		if cmd.Command.Get() != testCase.Command {
			t.Fail()
			t.Logf("Input '%s' did not generate the correct command expected %d got %d", testCase.Input, testCase.Command, cmd.Command.Get())
		}
		if cmd.Revision != testCase.Revision {
			t.Fail()
			t.Logf("Unexpected revision on command '%s', got %d while expecting %d", testCase.Input, cmd.Revision, testCase.Revision)
		}
	}
}

func TestNds2ParseSetEpoch(t *testing.T) {
	type TestData struct {
		Input string
		Epoch common.TimeSpan
	}

	TestsCases := []TestData{
		{"set-epoch 0-1999999999;", common.CreateTimeSpan(0, common.GpsInf)},
		{"set-epoch 1000000000-1999999999;", common.CreateTimeSpan(1000000000, common.GpsInf)},
		{"set-epoch 1000000000-1000000200;", common.CreateTimeSpan(1000000000, 1000000200)},
	}

	for _, testCase := range TestsCases {
		cmd := ParseCommand(testCase.Input)
		if cmd.Command.Get() != CommandSetEpoch {
			t.Fail()
			t.Logf("Input '%s' did not generate the correct command expected %d got %d", testCase.Input, CommandSetEpoch, cmd.Command.Get())
		}
		if cmd.TimeSpan != testCase.Epoch {
			t.Fail()
			t.Logf("Unexpected revision on command '%s', got [%d, %d) while expecting [%d, %d)", testCase.Input,
				cmd.TimeSpan.Start(), cmd.TimeSpan.End(),
				testCase.Epoch.Start(), testCase.Epoch.End())
		}
	}
}

func TestNds2ParseCountChannels(t *testing.T) {
	type TestData struct {
		Input     string
		Time      common.GpsSecond
		ClassType byte
		Pattern   string
	}

	TestCases := []TestData{
		{"count-channels 5 unknown;", 5, common.ClassTypeUnknown, ""},
		{"count-channels 5 online;", 5, common.ClassTypeOnline, ""},
		{"count-channels 0 raw;", 0, common.ClassTypeRaw, ""},
		{"count-channels 0 reduced;", 0, common.ClassTypeReduced, ""},
		{"count-channels 5 s-trend;", 5, common.ClassTypeSTrend, ""},
		{"count-channels 5 m-trend;", 5, common.ClassTypeMTrend, ""},
		{"count-channels 5 static;", 5, common.ClassTypeStatic, ""},
		{"count-channels 6 test-pt;", 6, common.ClassTypeTestPt, ""},
		{"count-channels 6 test-pt {?1:*PEM*};", 6, common.ClassTypeTestPt, "{?1:*PEM*}"},
		{"count-channels 6 test-pt {?1:*{PEM,SUS}*{}{ABC,{D*F}}};", 6, common.ClassTypeTestPt, "{?1:*{PEM,SUS}*{}{ABC,{D*F}}}"},
		{"count-channels 42 test-pt {?1:*{PE\\{\\}M,SUS}\\,*{}{ABC,{D*F}}};", 42, common.ClassTypeTestPt, "{?1:*{PE\\{\\}M,SUS}\\,*{}{ABC,{D*F}}}"},
	}
	for _, testCase := range TestCases {
		cmd := ParseCommand(testCase.Input)
		if cmd.Command.Get() != CommandCountChannels ||
			cmd.ClassType.Type != testCase.ClassType ||
			cmd.GpsTime != testCase.Time /* || cmd.Pattern.Pattern != testCase.Pattern */ {
			t.Fail()
			t.Logf("Failed on '%s' got %d %d %d %s", testCase.Input, cmd.Command.Get(), cmd.ClassType.Type, cmd.GpsTime, cmd.Pattern)
			t.Logf("expecting %d %d %d %s", CommandCountChannels, testCase.ClassType, testCase.Time, testCase.Pattern)
		}
	}
	cmd := ParseCommand("count-channels 6 unknown {?1:*{PEM|SUS}*{}{ABC|{D*F}")
	if cmd.Command.Get() != CommandUnknown {
		t.Fail()
		t.Log("The test did not properly fail '{?1:*{PEM|SUS}*{}{ABC|{D*F}' with unbalanced braces")
	}
}

func TestNds2ParseGetChannels(t *testing.T) {
	type TestData struct {
		Input     string
		Time      common.GpsSecond
		ClassType byte
		Pattern   string
	}

	TestCases := []TestData{
		{"get-channels 5 unknown;", 5, common.ClassTypeUnknown, ""},
		{"get-channels 5 online;", 5, common.ClassTypeOnline, ""},
		{"get-channels 0 raw;", 0, common.ClassTypeRaw, ""},
		{"get-channels 0 reduced;", 0, common.ClassTypeReduced, ""},
		{"get-channels 5 s-trend;", 5, common.ClassTypeSTrend, ""},
		{"get-channels 5 m-trend;", 5, common.ClassTypeMTrend, ""},
		{"get-channels 5 static;", 5, common.ClassTypeStatic, ""},
		{"get-channels 6 test-pt;", 6, common.ClassTypeTestPt, ""},
		{"get-channels 6 test-pt {?1:*PEM*};", 6, common.ClassTypeTestPt, "{?1:*PEM*}"},
		{"get-channels 6 test-pt {?1:*{PEM,SUS}*{}{ABC,{D*F}}};", 6, common.ClassTypeTestPt, "{?1:*{PEM,SUS}*{}{ABC,{D*F}}}"},
		{"get-channels 42 test-pt {?1:*{PE\\{\\}M,SUS}\\,*{}{ABC,{D*F}}};", 42, common.ClassTypeTestPt, "{?1:*{PE\\{\\}M,SUS}\\,*{}{ABC,{D*F}}}"},
	}
	for _, testCase := range TestCases {
		cmd := ParseCommand(testCase.Input)
		if cmd.Command.Get() != CommandGetChannels ||
			cmd.ClassType.Type != testCase.ClassType ||
			cmd.GpsTime != testCase.Time /* || cmd.Pattern != testCase.Pattern */ {
			t.Fail()
			t.Logf("Failed on '%s' got %d %d %d %s", testCase.Input, cmd.Command.Get(), cmd.ClassType.Type, cmd.GpsTime, cmd.Pattern)
			t.Logf("expecting %d %d %d %s", CommandGetChannels, testCase.ClassType, testCase.Time, testCase.Pattern)
		}
	}
	cmd := ParseCommand("get-channels 6 unknown {?1:*{PEM|SUS}*{}{ABC|{D*F}")
	if cmd.Command.Get() != CommandUnknown {
		t.Fail()
		t.Log("The test did not properly fail '{?1:*{PEM|SUS}*{}{ABC|{D*F}' with unbalanced braces")
	}
}

func TestNds2ParseGetChannelsCrc(t *testing.T) {
	type TestData struct {
		Input     string
		Time      common.GpsSecond
		ClassType byte
		Pattern   string
		Prefix    string
	}

	TestCases := []TestData{
		{"get-channel-crc 5 unknown;", 5, common.ClassTypeUnknown, "", ""},
		{"get-channel-crc 5 unknown {X1:PEM*};", 5, common.ClassTypeUnknown, "^X1:PEM.*$", "X1:PEM"},
		{"get-channel-crc 5 unknown {X?:PEM*};", 5, common.ClassTypeUnknown, "^X.:PEM.*$", "X"},
		{"get-channel-crc 5 unknown {X1:{PEMA,PEMB}*};", 5, common.ClassTypeUnknown, "^X1:(PEMA|PEMB).*$", "X1:"},
		{"get-channel-crc 5 unknown {{X1:PEMA,X1:PEMB}*};", 5, common.ClassTypeUnknown, "^(X1:PEMA|X1:PEMB).*$", ""},
		{"get-channel-crc 5 online;", 5, common.ClassTypeOnline, "", ""},
		{"get-channel-crc 0 raw;", 0, common.ClassTypeRaw, "", ""},
		{"get-channel-crc 0 reduced;", 0, common.ClassTypeReduced, "", ""},
		{"get-channel-crc 5 s-trend;", 5, common.ClassTypeSTrend, "", ""},
		{"get-channel-crc 5 m-trend;", 5, common.ClassTypeMTrend, "", ""},
		{"get-channel-crc 5 static;", 5, common.ClassTypeStatic, "", ""},
		{"get-channel-crc 6 test-pt;", 6, common.ClassTypeTestPt, "", ""},
	}
	for _, testCase := range TestCases {
		cmd := ParseCommand(testCase.Input)
		if cmd.Command.Get() != CommandGetChannelCrc ||
			cmd.ClassType.Type != testCase.ClassType ||
			cmd.GpsTime != testCase.Time || cmd.Pattern.RePattern != testCase.Pattern {
			t.Fail()
			t.Logf("Failed on '%s' got %d %d %d \n'%s'", testCase.Input, cmd.Command.Get(), cmd.ClassType.Type, cmd.GpsTime, cmd.Pattern.RePattern)
			t.Logf("expecting %d %d %d \n'%s'", CommandGetChannelCrc, testCase.ClassType, testCase.Time, testCase.Pattern)
		}
		if cmd.Pattern.CommonPrefix != testCase.Prefix {
			t.Fail()
			t.Logf("Wrong common prefix extracted on '%s', expected '%s' got '%s'", testCase.Input, testCase.Prefix, cmd.Pattern.CommonPrefix)
		}
	}
	cmd := ParseCommand("get-channel-crc 6 unknown {?1:*{PEM|SUS}*{}{ABC|{D*F}")
	if cmd.Command.Get() != CommandUnknown {
		t.Fail()
		t.Log("The test did not properly fail '{?1:*{PEM|SUS}*{}{ABC|{D*F}' with unbalanced braces")
	}
}

func TestNds2ParseGetSourceData(t *testing.T) {
	cmd := ParseCommand("get-source-data 0 {H1:CHAN-1};")
	if cmd.Command.Get() != CommandGetSourceData {
		t.Log("Wrong command type not get-source-data")
		t.Fail()
	}
	if cmd.GpsTime != 0 {
		t.Logf("Wrong gps time, '%d' instead of 0", cmd.GpsTime)
		t.Fail()
	}
	if len(cmd.Channels) != 1 {
		t.Fatalf("Wrong channel list length %d instead of 1", len(cmd.Channels))
	}
	if cmd.Channels[0].Name != "H1:CHAN-1" || cmd.Channels[0].Class.Type != common.ClassTypeUnknown || cmd.Channels[0].Rate != 0.0 {
		t.Logf("Wrong channel metadata '%s' %d %f instead of 'H1:CHAN-1' %d 0.0", cmd.Channels[0].Name, cmd.Channels[0].Class.Type, cmd.Channels[0].Rate, common.ClassTypeUnknown)
	}

	cmd = ParseCommand("get-source-data 42 {H1:CHAN/1,raw%16 H1:CHAN~2%real_4,int_4%0.5,m-trend%reduced%64};")
	if cmd.Command.Get() != CommandGetSourceData {
		t.Log("Wrong command type not get-source-data")
		t.Fail()
	}
	if cmd.GpsTime != 42 {
		t.Logf("Wrong gps time, '%d' instead of 42", cmd.GpsTime)
		t.Fail()
	}
	if len(cmd.Channels) != 2 {
		t.Fatalf("Wrong channel list length %d instead of 2", len(cmd.Channels))
	}
	if cmd.Channels[0].Name != "H1:CHAN/1" || cmd.Channels[0].Class.Type != common.ClassTypeRaw || cmd.Channels[0].Type.Type != common.DataTypeUnknown || cmd.Channels[0].Rate != 16.0 {
		t.Logf("Wrong channel metadata '%s' %d %d %f instead of 'H1:CHAN/1' %d %d 16.0", cmd.Channels[0].Name, cmd.Channels[0].Class.Type, cmd.Channels[0].Type.Type, cmd.Channels[0].Rate, common.ClassTypeRaw, common.DataTypeUnknown)
		t.Fail()
	}
	if cmd.Channels[1].Name != "H1:CHAN~2" || cmd.Channels[1].Class.Type != common.ClassTypeReduced || cmd.Channels[1].Type.Type != common.DataTypeInt32 || cmd.Channels[1].Rate != 64.0 {
		t.Logf("Wrong channel metadata '%s' %d %d %f instead of 'H1:CHAN~2' %d %d 64.0", cmd.Channels[1].Name, cmd.Channels[1].Class.Type, cmd.Channels[1].Type.Type, cmd.Channels[1].Rate, common.ClassTypeReduced, common.DataTypeInt32)
		t.Fail()
	}
}

func TestNds2ParseGetSourceList(t *testing.T) {
	cmd := ParseCommand("get-source-list 0 {H1:CHAN-1};")
	if cmd.Command.Get() != CommandGetSourceList {
		t.Log("Wrong command type not get-source-list")
		t.Fail()
	}
	if cmd.GpsTime != 0 {
		t.Logf("Wrong gps time, '%d' instead of 0", cmd.GpsTime)
		t.Fail()
	}
	if len(cmd.Channels) != 1 {
		t.Fatalf("Wrong channel list length %d instead of 1", len(cmd.Channels))
	}
	if cmd.Channels[0].Name != "H1:CHAN-1" || cmd.Channels[0].Class.Type != common.ClassTypeUnknown || cmd.Channels[0].Rate != 0.0 {
		t.Logf("Wrong channel metadata '%s' %d %f instead of 'H1:CHAN-1' %d 0.0", cmd.Channels[0].Name, cmd.Channels[0].Class.Type, cmd.Channels[0].Rate, common.ClassTypeUnknown)
	}

	cmd = ParseCommand("get-source-list 42 {H1:CHAN/1,raw%16 H1:CHAN~2%real_4,int_4%0.5,m-trend%reduced%64};")
	if cmd.Command.Get() != CommandGetSourceList {
		t.Log("Wrong command type not get-source-list")
		t.Fail()
	}
	if cmd.GpsTime != 42 {
		t.Logf("Wrong gps time, '%d' instead of 42", cmd.GpsTime)
		t.Fail()
	}
	if len(cmd.Channels) != 2 {
		t.Fatalf("Wrong channel list length %d instead of 2", len(cmd.Channels))
	}
	if cmd.Channels[0].Name != "H1:CHAN/1" || cmd.Channels[0].Class.Type != common.ClassTypeRaw || cmd.Channels[0].Type.Type != common.DataTypeUnknown || cmd.Channels[0].Rate != 16.0 {
		t.Logf("Wrong channel metadata '%s' %d %d %f instead of 'H1:CHAN/1' %d %d 16.0", cmd.Channels[0].Name, cmd.Channels[0].Class.Type, cmd.Channels[0].Type.Type, cmd.Channels[0].Rate, common.ClassTypeRaw, common.DataTypeUnknown)
		t.Fail()
	}
	if cmd.Channels[1].Name != "H1:CHAN~2" || cmd.Channels[1].Class.Type != common.ClassTypeReduced || cmd.Channels[1].Type.Type != common.DataTypeInt32 || cmd.Channels[1].Rate != 64.0 {
		t.Logf("Wrong channel metadata '%s' %d %d %f instead of 'H1:CHAN~2' %d %d 64.0", cmd.Channels[1].Name, cmd.Channels[1].Class.Type, cmd.Channels[1].Type.Type, cmd.Channels[1].Rate, common.ClassTypeReduced, common.DataTypeInt32)
		t.Fail()
	}
}
