package main

import (
	"errors"
	"log"
	"net/http"
	"os"
	"path"
	"regexp"
	"sync"
	"time"
)

type ServerFile struct {
	FrameType  string
	CacheFile  string
	SourceFile string
	Checksum   string
	TimeStamp  time.Time
}

func serverCopyAndChecksum(source, dest string) (string, error) {
	input, err := os.Open(source)
	if err != nil {
		return "", err
	}
	defer input.Close()
	return copyAndChecksum(input, dest, true)
}

func processForServer(cfg *ServerConfig, filename string) (ServerFile, error) {

	frameType := serverGetFrameType(filename)
	if frameType == "" {
		return ServerFile{}, errors.New("invalid filename")
	}
	cachePath := path.Join(cfg.CacheDir, filename+".gz")
	sourceFile := path.Join(cfg.SourceDir, filename)
	sourceInfo, err := os.Stat(sourceFile)
	if err != nil {
		return ServerFile{}, err
	}
	hashString, err := serverCopyAndChecksum(sourceFile, cachePath)
	if err != nil {
		log.Printf("Error processing %s, %v", sourceFile, err)
		return ServerFile{}, err
	}
	log.Printf("Cached %s - %s", sourceFile, hashString)
	return ServerFile{
		FrameType:  frameType,
		CacheFile:  cachePath,
		SourceFile: sourceFile,
		Checksum:   hashString,
		TimeStamp:  sourceInfo.ModTime(),
	}, nil
}

func processForServerIfNewer(oldEntry ServerFile) (ServerFile, error) {
	sourceInfo, err := os.Stat(oldEntry.SourceFile)
	if err != nil {
		return ServerFile{}, err
	}
	if !sourceInfo.ModTime().After(oldEntry.TimeStamp) {
		return oldEntry, nil
	}
	hashString, err := serverCopyAndChecksum(oldEntry.SourceFile, oldEntry.CacheFile)
	if err != nil {
		log.Printf("Error processing %s, %v", oldEntry.SourceFile, err)
		return ServerFile{}, err
	}
	log.Printf("Refreshed cache for %s - %s", oldEntry.SourceFile, hashString)

	return ServerFile{
		FrameType:  oldEntry.FrameType,
		CacheFile:  oldEntry.CacheFile,
		SourceFile: oldEntry.SourceFile,
		Checksum:   hashString,
		TimeStamp:  sourceInfo.ModTime(),
	}, nil
}

var serverTextRe *regexp.Regexp

func init() {
	serverTextRe = regexp.MustCompile("^(?P<frameType>[A-Z0-9a-z\\-_]+)-ChanList.txt$")
}

func serverGetFrameType(filename string) string {
	return serverTextRe.FindStringSubmatch(filename)[1]
}

func runServer(cfg *ServerConfig) {

	requireDirectory(cfg.CacheDir)
	requireDirectory(cfg.SourceDir)

	log.Printf("Scanning %s for *ChanList.txt files", cfg.SourceDir)
	files, err := getFilenames(cfg.SourceDir, reFilePredicate(serverTextRe))
	if err != nil {
		log.Fatalf("Error while scanning sources, %v", err)
	}

	trackingList := make(map[string]ServerFile)
	var lock sync.Mutex

	for _, fileName := range files {
		entry, _ := processForServer(cfg, fileName)
		trackingList[entry.FrameType] = entry
	}

	doneSignal := make(chan bool)
	wg := sync.WaitGroup{}
	wg.Add(2)
	go func() {
		defer wg.Done()
		defer func() {
			doneSignal <- true
		}()
		serverHttpLoop(cfg.ListenAddress, &trackingList, &lock)
	}()
	go func() {
		defer wg.Done()
		serverUpdateLoop(doneSignal, &trackingList, &lock)
	}()
	wg.Wait()
}

func serverHttpLoop(listenAddress string, tracking *map[string]ServerFile, lock *sync.Mutex) {
	mux := http.NewServeMux()
	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if r.Method != "GET" {
			http.Error(w, "Method not supported", http.StatusMethodNotAllowed)
			return
		}
		_ = r.ParseForm()

		frameType := r.URL.Path[1:]
		lock.Lock()
		defer lock.Unlock()

		serverFile, ok := (*tracking)[frameType]
		if !ok {
			http.Error(w, "Not found", http.StatusNotFound)
			return
		}
		queryChecksum := r.Form.Get("checksum")
		if queryChecksum == serverFile.Checksum {
			w.WriteHeader(http.StatusNotModified)
			return
		}
		log.Printf("Serving channel list for '%s'", frameType)
		http.ServeFile(w, r, serverFile.CacheFile)
	})
	log.Printf("Listening on %s", listenAddress)
	err := http.ListenAndServe(listenAddress, mux)
	if err != nil {
		log.Print(err)
	}
}

func serverUpdateLoop(doneSignal chan bool, tracking *map[string]ServerFile, lock *sync.Mutex) {
	frameTypes := func() []string {
		lock.Lock()
		defer lock.Unlock()
		keys := make([]string, 0, len(*tracking))
		for k, _ := range *tracking {
			keys = append(keys, k)
		}
		return keys
	}()

	timer := time.NewTimer(waitTime())
	defer func() {
		if !timer.Stop() {
			<-timer.C
		}
	}()
	for {
		select {
		case <-doneSignal:
			return
		case <-timer.C:
			break
		}
		for _, frameType := range frameTypes {
			func() {
				lock.Lock()
				defer lock.Unlock()

				oldEntry, ok := (*tracking)[frameType]
				if !ok {
					return
				}
				newEntry, err := processForServerIfNewer(oldEntry)
				if err != nil {
					return
				}
				(*tracking)[frameType] = newEntry
			}()
		}
		timer.Reset(waitTime())
	}
}
