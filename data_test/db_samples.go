/*
 The data_test package is a collection of simple database streams used for testing purposes.
 It is not meant to be used in non-test code.
*/
package data_test

import (
	"bytes"
	"io"
)

// sample channel list from a H1 C00 frame
func SampleDBReplicationStream1() []byte {
	return []byte(`{"num_channels":12,"frame_types":["H-H1_HOFT_C00"],"frame_source":"null_frames://null"}
{"c":{"n":"H1:DMT-DQ_VECTOR","dt":64,"ct":4,"r":16},"a":[{"frame":0,"t":[1000000000,1500000000]}]}
{"c":{"n":"H1:GDS-CALIB_F_CC","dt":8,"ct":4,"r":16},"a":[{"frame":0,"t":[1000000000,1500000000]}]}
{"c":{"n":"H1:GDS-CALIB_KAPPA_A_IMAGINARY","dt":8,"ct":4,"r":16},"a":[{"frame":0,"t":[1000000000,1500000000]}]}
{"c":{"n":"H1:GDS-CALIB_KAPPA_A_REAL","dt":8,"ct":4,"r":16},"a":[{"frame":0,"t":[1000000000,1100000000]}]}
{"c":{"n":"H1:GDS-CALIB_KAPPA_C","dt":8,"ct":4,"r":16},"a":[{"frame":0,"t":[1000000000,1100000000]}]}
{"c":{"n":"H1:GDS-CALIB_KAPPA_PU_IMAGINARY","dt":8,"ct":4,"r":16},"a":[{"frame":0,"t":[1000000000,1100000000]}]}
{"c":{"n":"H1:GDS-CALIB_KAPPA_PU_REAL","dt":8,"ct":4,"r":16},"a":[{"frame":0,"t":[1000000000,1100000000]}]}
{"c":{"n":"H1:GDS-CALIB_KAPPA_TST_IMAGINARY","dt":8,"ct":4,"r":16},"a":[{"frame":0,"t":[1000000000,1100000000]}]}
{"c":{"n":"H1:GDS-CALIB_KAPPA_TST_REAL","dt":8,"ct":4,"r":16},"a":[{"frame":0,"t":[1000000000,1100000000]}]}
{"c":{"n":"H1:GDS-CALIB_STATE_VECTOR","dt":64,"ct":4,"r":16},"a":[{"frame":0,"t":[1000000000,1100000000]}]}
{"c":{"n":"H1:GDS-CALIB_STRAIN","dt":16,"ct":4,"r":16384},"a":[{"frame":0,"t":[1000000000,1100000000]}]}
{"c":{"n":"H1:ODC-MASTER_CHANNEL_OUT_DQ","dt":64,"ct":4,"r":16384},"a":[{"frame":0,"t":[1050000000,1500000000]}]}
[{"frame_type":"H-H1_HOFT_C00","timespan":[1000000000,1200000000]}, {"frame_type":"H-H1_HOFT_C00","timespan":[1300000000,1400000000]}, {"frame_type":"H-H1_HOFT_C00","timespan":[1450000000,1500000000]}]
`)
}

func SampleDbReplicationStream1Reader() io.Reader {
	return bytes.NewReader(SampleDBReplicationStream1())
}

// sample channel list from a L1 C00 frame
func SampleDBReplicationStream2() []byte {
	return []byte(`{"num_channels":14,"frame_types":["L-L1_HOFT_C00", "L-L1_R"],"frame_source":"null_frames://null"}
{"c":{"n":"L1:DMT-DQ_VECTOR","dt":64,"ct":4,"r":16},"a":[{"frame":0,"t":[1000000000,1500000000]}]}
{"c":{"n":"L1:GDS-CALIB_F_CC","dt":8,"ct":4,"r":16},"a":[{"frame":0,"t":[1000000000,1500000000]}]}
{"c":{"n":"L1:GDS-CALIB_KAPPA_A_IMAGINARY","dt":8,"ct":4,"r":16},"a":[{"frame":0,"t":[1000000000,1500000000]}]}
{"c":{"n":"L1:GDS-CALIB_KAPPA_A_REAL","dt":8,"ct":4,"r":16},"a":[{"frame":0,"t":[1000000000,1100000000]}]}
{"c":{"n":"L1:GDS-CALIB_KAPPA_C","dt":8,"ct":4,"r":16},"a":[{"frame":0,"t":[1000000000,1100000000]}]}
{"c":{"n":"L1:GDS-CALIB_KAPPA_PU_IMAGINARY","dt":8,"ct":4,"r":16},"a":[{"frame":0,"t":[1000000000,1100000000]}]}
{"c":{"n":"L1:GDS-CALIB_KAPPA_PU_REAL","dt":8,"ct":4,"r":16},"a":[{"frame":0,"t":[1000000000,1100000000]}]}
{"c":{"n":"L1:GDS-CALIB_KAPPA_TST_IMAGINARY","dt":8,"ct":4,"r":16},"a":[{"frame":0,"t":[1000000000,1100000000]}]}
{"c":{"n":"L1:GDS-CALIB_KAPPA_TST_REAL","dt":8,"ct":4,"r":16},"a":[{"frame":0,"t":[1000000000,1100000000]}]}
{"c":{"n":"L1:GDS-CALIB_STATE_VECTOR","dt":64,"ct":4,"r":16},"a":[{"frame":0,"t":[1000000000,1100000000]}]}
{"c":{"n":"L1:GDS-CALIB_STRAIN","dt":16,"ct":4,"r":16384},"a":[{"frame":0,"t":[1000000000,1100000000]}]}
{"c":{"n":"L1:ODC-MASTER_CHANNEL_OUT_DQ","dt":64,"ct":4,"r":16384},"a":[{"frame":0,"t":[1050000000,1500000000]}]}
{"c":{"n":"L1:DAQ_CHANNEL_COUNT","dt":2,"ct":2,"r":16},"a":[{"frame":1,"t":[1050000000,1500000000]}]}
{"c":{"n":"L1:DAQ_DATA_RATE_MB","dt":2,"ct":2,"r":16},"a":[{"frame":1,"t":[1000000000,1500000000]}]}
[{"frame_type":"L-L1_HOFT_C00","timespan":[1000000000,1200000000]}, {"frame_type":"L-L1_HOFT_C00","timespan":[1300000000,1400000000]}, {"frame_type":"L-L1_HOFT_C00","timespan":[1450000000,1500000000]}]
[{"frame_type":"L-L1_R","timespan":[1000000000,1400000000]}, {"frame_type":"L-L1_R","timespan":[1450000000,1500000000]}]
`)
}

func SampleDbReplicationStream2Reader() io.Reader {
	return bytes.NewReader(SampleDBReplicationStream2())
}

// sample channel list from a L1 C00 frame + some H1 C00 channels
func SampleDBReplicationStream3() []byte {
	return []byte(`{"num_channels":14,"frame_types":["L-L1_HOFT_C00", "L-L1_R", "H-H1_HOFT_C00"],"frame_source":"null_frames://null"}
{"c":{"n":"H1:DMT-DQ_VECTOR","dt":64,"ct":4,"r":16},"a":[{"frame":2,"t":[1000000000,1500000000]}]}
{"c":{"n":"H1:GDS-CALIB_F_CC","dt":8,"ct":4,"r":16},"a":[{"frame":2,"t":[1000000000,1500000000]}]}
{"c":{"n":"L1:DMT-DQ_VECTOR","dt":64,"ct":4,"r":16},"a":[{"frame":0,"t":[1000000000,1500000000]}]}
{"c":{"n":"L1:GDS-CALIB_F_CC","dt":8,"ct":4,"r":16},"a":[{"frame":0,"t":[1000000000,1500000000]}]}
{"c":{"n":"L1:GDS-CALIB_KAPPA_A_IMAGINARY","dt":8,"ct":4,"r":16},"a":[{"frame":0,"t":[1000000000,1500000000]}]}
{"c":{"n":"L1:GDS-CALIB_KAPPA_A_REAL","dt":8,"ct":4,"r":16},"a":[{"frame":0,"t":[1000000000,1100000000]}]}
{"c":{"n":"L1:GDS-CALIB_KAPPA_C","dt":8,"ct":4,"r":16},"a":[{"frame":0,"t":[1000000000,1100000000]}]}
{"c":{"n":"L1:GDS-CALIB_KAPPA_PU_IMAGINARY","dt":8,"ct":4,"r":16},"a":[{"frame":0,"t":[1000000000,1100000000]}]}
{"c":{"n":"L1:GDS-CALIB_KAPPA_PU_REAL","dt":8,"ct":4,"r":16},"a":[{"frame":0,"t":[1000000000,1100000000]}]}
{"c":{"n":"L1:GDS-CALIB_STATE_VECTOR","dt":64,"ct":4,"r":16},"a":[{"frame":0,"t":[1000000000,1100000000]}]}
{"c":{"n":"L1:GDS-CALIB_STRAIN","dt":16,"ct":4,"r":16384},"a":[{"frame":0,"t":[1000000000,1100000000]}]}
{"c":{"n":"L1:ODC-MASTER_CHANNEL_OUT_DQ","dt":64,"ct":4,"r":16384},"a":[{"frame":0,"t":[1050000000,1500000000]}]}
{"c":{"n":"L1:DAQ_CHANNEL_COUNT","dt":2,"ct":2,"r":16},"a":[{"frame":1,"t":[1050000000,1500000000]}]}
{"c":{"n":"L1:DAQ_DATA_RATE_MB","dt":2,"ct":2,"r":16},"a":[{"frame":1,"t":[1000000000,1500000000]}]}
[{"frame_type":"L-L1_HOFT_C00","timespan":[1000000000,1200000000]}, {"frame_type":"L-L1_HOFT_C00","timespan":[1300000000,1400000000]}, {"frame_type":"L-L1_HOFT_C00","timespan":[1450000000,1500000000]}]
[{"frame_type":"L-L1_R","timespan":[1000000000,1400000000]}, {"frame_type":"L-L1_R","timespan":[1450000000,1500000000]}]
[{"frame_type":"H-H1_HOFT_C00","timespan":[1000000000,1200000000]}, {"frame_type":"H-H1_HOFT_C00","timespan":[1300000000,1400000000]}, {"frame_type":"H-H1_HOFT_C00","timespan":[1450000000,1500000000]}]
`)
}

func SampleDbReplicationStream3Reader() io.Reader {
	return bytes.NewReader(SampleDBReplicationStream3())
}

func SampleChanList2FromReader() io.Reader {
	return bytes.NewReader([]byte(`# Channel list:
# Name  type  access d-type rate [Sources]
H1:DMT-DQ_VECTOR reduced 0 uint_4 16 H-H1_HOFT_C00 1155063808:844936191
H1:DMT-DQ_VECTOR_GATED reduced 0 uint_4 16 H-H1_HOFT_C00 1228609020:771390979
H1:GDS-CALIB_F_CC reduced 0 real_4 16 H-H1_HOFT_C00 1124835328:98390164 H-H1_HOFT_C00 1225240576:774759423
H1:GDS-CALIB_F_CC_NOGATE reduced 0 real_4 16 H-H1_HOFT_C00 1162673012:60552480 H-H1_HOFT_C00 1225240576:774759423
H1:GDS-CALIB_F_S reduced 0 real_4 16 H-H1_HOFT_C00 1175978240:1203368 H-H1_HOFT_C00 1181412580:41812912 H-H1_HOFT_C00 1225240576:11838013
H1:GDS-CALIB_F_S_NOGATE reduced 0 real_4 16 H-H1_HOFT_C00 1175978240:1203368 H-H1_HOFT_C00 1181412580:41812912 H-H1_HOFT_C00 1225240576:11838013
H1:GDS-CALIB_F_S_SQUARED reduced 0 real_4 16 H-H1_HOFT_C00 1237078589:762921410
H1:GDS-CALIB_F_S_SQUARED_NOGATE reduced 0 real_4 16 H-H1_HOFT_C00 1237078589:762921410
H1:GDS-CALIB_GAMMA_IMAGINARY reduced 0 real_4 16 H-H1_HOFT_C00 1117383512:7451816
H1:GDS-CALIB_GAMMA_REAL reduced 0 real_4 16 H-H1_HOFT_C00 1117383512:7451816
H1:GDS-CALIB_KAPPA_A_IMAGINARY reduced 0 real_4 16 H-H1_HOFT_C00 1124835328:37837684
H1:GDS-CALIB_KAPPA_A_REAL reduced 0 real_4 16 H-H1_HOFT_C00 1124835328:37837684
H1:GDS-CALIB_KAPPA_C reduced 0 real_4 16 H-H1_HOFT_C00 1124835328:98390164 H-H1_HOFT_C00 1225240576:774759423
H1:GDS-CALIB_KAPPA_C_NOGATE reduced 0 real_4 16 H-H1_HOFT_C00 1162673012:60552480 H-H1_HOFT_C00 1225240576:774759423
H1:GDS-CALIB_KAPPA_PUM_IMAGINARY reduced 0 real_4 16 H-H1_HOFT_C00 1225240576:774759423
H1:GDS-CALIB_KAPPA_PUM_IMAGINARY_NOGATE reduced 0 real_4 16 H-H1_HOFT_C00 1225240576:774759423
H1:GDS-CALIB_KAPPA_PUM_REAL reduced 0 real_4 16 H-H1_HOFT_C00 1225240576:774759423
H1:GDS-CALIB_KAPPA_PUM_REAL_NOGATE reduced 0 real_4 16 H-H1_HOFT_C00 1225240576:774759423
H1:GDS-CALIB_KAPPA_PU_IMAGINARY reduced 0 real_4 16 H-H1_HOFT_C00 1124835328:98390164
H1:GDS-CALIB_KAPPA_PU_IMAGINARY_NOGATE reduced 0 real_4 16 H-H1_HOFT_C00 1162673012:60552480
H1:GDS-CALIB_KAPPA_PU_REAL reduced 0 real_4 16 H-H1_HOFT_C00 1124835328:98390164
H1:GDS-CALIB_KAPPA_PU_REAL_NOGATE reduced 0 real_4 16 H-H1_HOFT_C00 1162673012:60552480
H1:GDS-CALIB_KAPPA_TST_IMAGINARY reduced 0 real_4 16 H-H1_HOFT_C00 1124835328:98390164 H-H1_HOFT_C00 1225240576:774759423
H1:GDS-CALIB_KAPPA_TST_IMAGINARY_NOGATE reduced 0 real_4 16 H-H1_HOFT_C00 1162673012:60552480 H-H1_HOFT_C00 1225240576:774759423
H1:GDS-CALIB_KAPPA_TST_REAL reduced 0 real_4 16 H-H1_HOFT_C00 1124835328:98390164 H-H1_HOFT_C00 1225240576:774759423
H1:GDS-CALIB_KAPPA_TST_REAL_NOGATE reduced 0 real_4 16 H-H1_HOFT_C00 1162673012:60552480 H-H1_HOFT_C00 1225240576:774759423
H1:GDS-CALIB_KAPPA_UIM_IMAGINARY reduced 0 real_4 16 H-H1_HOFT_C00 1225240576:774759423
H1:GDS-CALIB_KAPPA_UIM_IMAGINARY_NOGATE reduced 0 real_4 16 H-H1_HOFT_C00 1225240576:774759423
H1:GDS-CALIB_KAPPA_UIM_REAL reduced 0 real_4 16 H-H1_HOFT_C00 1228693504:771306495
H1:GDS-CALIB_KAPPA_UIM_REAL_NOGATE reduced 0 real_4 16 H-H1_HOFT_C00 1225240576:774759423
H1:GDS-CALIB_SRC_Q_INVERSE reduced 0 real_4 16 H-H1_HOFT_C00 1175978240:1203368 H-H1_HOFT_C00 1181412580:41812912 H-H1_HOFT_C00 1225240576:774759423
H1:GDS-CALIB_SRC_Q_INVERSE_NOGATE reduced 0 real_4 16 H-H1_HOFT_C00 1175978240:1203368 H-H1_HOFT_C00 1181412580:41812912 H-H1_HOFT_C00 1225240576:774759423
H1:GDS-CALIB_STATE_VECTOR reduced 0 uint_4 16 H-H1_HOFT_C00 1102863616:897136383
H1:GDS-CALIB_STRAIN reduced 0 real_8 16384 H-H1_HOFT_C00 1113333252:886666747
H1:GDS-CALIB_STRAIN_CLEAN reduced 0 real_8 16384 H-H1_HOFT_C00 1228609020:771390979
H1:GDS-FAKE_STRAIN reduced 0 real_8 16384 H-H1_HOFT_C00 1102863616:10469636
H1:GDS-GATED_STRAIN reduced 0 real_8 16384 H-H1_HOFT_C00 1228609020:771390979
H1:IDQ-EFF_OVL_16_4096 reduced 0 real_8 128 H-H1_HOFT_C00 1235857408:764142591
H1:IDQ-FAP_OVL_16_4096 reduced 0 real_8 128 H-H1_HOFT_C00 1235857408:764142591
H1:IDQ-LOGLIKE_OVL_16_4096 reduced 0 real_8 128 H-H1_HOFT_C00 1235857408:764142591
H1:IDQ-OK_OVL_16_4096 reduced 0 real_8 128 H-H1_HOFT_C00 1235857408:764142591
H1:IDQ-PGLITCH_OVL_16_4096 reduced 0 real_8 128 H-H1_HOFT_C00 1235857408:764142591
H1:IDQ-RANK_OVL_16_4096 reduced 0 real_8 128 H-H1_HOFT_C00 1235857408:764142591
H1:ODC-MASTER_CHANNEL_OUT_DQ reduced 0 uint_4 16384 H-H1_HOFT_C00 1113333252:113550960
`))
}

// sample channel list from a H1 C00 frame
func SampleDBReplicationStream4() []byte {
	return []byte(`{"num_channels":9,"frame_types":["FT1", "FT2"],"frame_source":"null_frames://null"}
{"c":{"n":"X2:CHAN1","dt":64,"ct":4,"r":16},"a":[{"frame":0,"t":[1100000000,1200000000]},{"frame":0,"t":[1300000000,1999999999]}, {"frame":1,"t":[1250000000,1999999999]}]}
{"c":{"n":"X2:CHAN2","dt":8,"ct":4,"r":16},"a":[{"frame":0,"t":[1000000000,1999999999]}]}
{"c":{"n":"X2:CHAN3","dt":8,"ct":4,"r":16},"a":[{"frame":0,"t":[1000000000,1500000000]}]}
{"c":{"n":"X2:CHAN4","dt":8,"ct":4,"r":16},"a":[{"frame":0,"t":[1000000000,1100000000]}]}
{"c":{"n":"X2:CHAN5","dt":8,"ct":4,"r":16},"a":[{"frame":0,"t":[1000000000,1100000000]}]}
{"c":{"n":"X2:CHAN6","dt":8,"ct":4,"r":16},"a":[{"frame":0,"t":[1000000000,1100000000]}]}
{"c":{"n":"X2:CHAN7","dt":8,"ct":4,"r":16},"a":[{"frame":0,"t":[1000000000,1100000000]}]}
{"c":{"n":"X2:CHAN8","dt":8,"ct":2,"r":16},"a":[{"frame":0,"t":[1000000000,1100000000]}]}
{"c":{"n":"X2:CHAN9","dt":8,"ct":2,"r":16},"a":[{"frame":0,"t":[0,1999999999]}]}
[{"frame_type":"FT1","timespan":[1000000000,1050000000]},{"frame_type":"FT1","timespan":[1060000000,1200000000]}, {"frame_type":"FT1","timespan":[1300000000,1400000000]}, {"frame_type":"FT1","timespan":[1450000000,1500000000]}]
[{"frame_type":"FT2","timespan":[1000000000,1200000000]},{"frame_type":"FT2","timespan":[1240000000,1350000000]}, {"frame_type":"FT2","timespan":[1380000000,1400000000]}, {"frame_type":"FT2","timespan":[1450000000,1500000000]}]
`)
}

func SampleDbReplicationStream4Reader() io.Reader {
	return bytes.NewReader(SampleDBReplicationStream4())
}
